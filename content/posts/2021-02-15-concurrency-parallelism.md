---
title: "Konkurensi vs Paralelisme"
date: 2021-02-15T13:13:46+07:00
draft: false

category: Programming
tags:
    - konkurensi
    - paralelisme
    - sistem operasi
keywords:
    - konkurensi
    - paralelisme
    - sistem operasi
---

Konkurensi dan Parelelisme adalah suatu pendekatan yg tujuanya sama-sama melakukan pekerjaan secara bersamaan dalam satu waktu. jadi apa yang membedakan?. Rob Pike, salah satu creator bahasa pemograman Golang pernah mengutip dalam suatu kesempatan.

"*Concurrency is about **dealing** with lots of things at once. Parallelism is about **doing** lots of things at once.*" - **Rob Pike**

Dari pernyataan tersebut, Konkurensi itu terkait dengan **bagaimana cara atau metodenya**,  supaya pekerjaan yang banyak bisa dikerjakan secara bersamaan, Sedangkan Parelelisme **bagaimana melakukan** banyak pekerjaan secara bersamaan sekaligus.

Masih bingung? coba perhatikan gambar berikut

![cuncurrency](https://techdifferences.com/wp-content/uploads/2017/12/Untitled.jpg)

Dari gambar diatas dijelaskan bahwa, Pekerjaan dikerjaan (diproses) secara bersamaan, tetapi **waktu eksekusi** yang membedakanya. Kenapa ada perbedaan jeda waktu eksekusi ? Coba perhatikan lagi gambar berikut

![Concurrency vs parallelism](https://miro.medium.com/max/409/1*_4B2PKsJn9pUz3jbTnBnYw.png)

Dari gambar diatas ada perbedaan jumlah **sumber daya** (mesin) yang melayani. Pendekatan konkurensi, melayani dengan satu mesin sedangkan pendekatan pararelisme menggunakan lebih dari satu mesin untuk jumlah permintaan yang sama. Secara logika pendekatan pararelisme ini tentunya bisa lebih cepat memproses dengan waktu proses yang lebih singkat. Namun hal ini tergantung pada
- Pekerjaan atau task yang akan diproses.
- Cara/metode yg dipakai masing-masing mesin (pemroses).

Jika tidak ada independensi antar task dan kemampuan masing-masing mesin berbeda justru akan berdampak sebaliknya.

##### Konkurensi:
- Terkait dengan **kemampuan dari sumber daya** untuk menangani objek. Sumber daya disini adalah sistem atau aplikasi yang ada di dalam mesin tersebut.
- Multitasking pada **satu logical prosesor (single-core)**.
- Konkurensi berkaitan dengan struktur perangkat lunak.  

##### Paralelisme:
- Terkait dengan **jumlah dari sumber daya** untuk memproses objek. Sumber daya disini adalah mesinnya.
- Multitasking pada **lebih dari satu logical prosesor (multi-core)**.
- Paralelisme berkaitan dengan perangkat keras.

### Kapan menggunakan pendekataan Konkurensi & kapan menggunakan paralisme ?
Jika kita bicara mengenai pendekataan mana yang lebih tepat digunakan adalah tergantung dari kasus yang akan dikerjaan, karena implementasi yang salah, justru bisa memperlambat pekerjaan.


### Implemetasi dalam bahasa pemograman

1. Playing with Erlang Concurrency - https://gorillalogic.com/blog/playing-with-erlang-concurrency/
2. Concurrency in Erlang & Scala: The Actor Model https://rocketeer.be/articles/concurrency-in-erlang-scala/

### Referensi

1. https://stackoverflow.com/questions/1050222/what-is-the-difference-between-concurrency-and-parallelism
