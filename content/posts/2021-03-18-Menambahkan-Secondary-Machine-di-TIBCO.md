---
title: "Menambahkan Tibco BW Secondary Machine di Linux"
date: 2021-03-18T08:18:31+07:00
draft: true

category: tutorial
tags:
    - tibco
    - domainutility
keywords:
    - tibco
---

## Prerequisite
- Karena penambahan menggunakan GUI, diharapkan both (cilent,server) sudah di config untuk bisa menjalankan X-Window
    - Dari sisi server
      1. Pastikan parameter `X11Forwarding Yes` di set `Yes` di `/etc/ssh/sshd_config`
      2. Sudah terinstall `xorg-x11-xauth` Lib
    - Dari sisi client
      1. Jika anda pengguna linux, tambahkan `-X` saat melakukan SSH, ex : `ssh -X user@ip-server`
      2. Jika anda pengguna window, bisa menggunakan mobaxterm (sudah built-in X-Window)

